package misc;
import logicMethods.Andd;
import logicMethods.Nott;
import logicMethods.Orr;
import logicMethods.Xorr;
import encoderDecoder.Decoder;
import encoderDecoder.Encoder;
import flipFlop.MSFlipFlop;
import latches.Latch;
import latches.LatchVari;
public class Test {

	public static void main(String[] args) {
		SelectVari variable = new SelectVari();
		String preferred =  variable.preferred;
		String other = variable.other;
		
		
		
		String result;
		
		//not Method
		System.out.println("Not Method:");
		result = Nott.not(other);
		System.out.println(other + "|" + result);
		result = Nott.not(preferred);
		System.out.println(preferred + "|" + result);
		System.out.println();
		
		
		//and method
		System.out.println("And Method:");
		result = Andd.and(other, other);
		System.out.println(other + " " + other + "|" + result);
		result = Andd.and(other, preferred);
		System.out.println(other + " " + preferred + "|" + result);
		result = Andd.and(preferred, other);
		System.out.println(preferred + " " + other + "|" + result);
		result = Andd.and(preferred, preferred);
		System.out.println(preferred + " " + preferred + "|" + result);
		System.out.println();
		
		//nand method
		System.out.println("Nand Method:");
		result = Andd.nand(other, other);
		System.out.println(other + " " + other + "|" + result);
		result = Andd.nand(other, preferred);
		System.out.println(other + " " + preferred + "|" + result);
		result = Andd.nand(preferred, other);
		System.out.println(preferred + " " + other + "|" + result);
		result = Andd.nand(preferred, preferred);
		System.out.println(preferred + " " + preferred + "|" + result);
		System.out.println();
		
		//or Method
		System.out.println("Or Method:");
		result = Orr.or(other, other);
		System.out.println(other + " " + other + "|" + result);
		result = Orr.or(other, preferred);
		System.out.println(other + " " + preferred + "|" + result);
		result = Orr.or(preferred, other);
		System.out.println(preferred + " " + other + "|" + result);
		result = Orr.or(preferred, preferred);
		System.out.println(preferred + " " + preferred + "|" + result);
		System.out.println();
		
		//nor method
		System.out.println("Nor Method:");
		result = Orr.nor(other, other);
		System.out.println(other + " " + other + "|" + result);
		result = Orr.nor(other, preferred);
		System.out.println(other + " " + preferred + "|" + result);
		result = Orr.nor(preferred, other);
		System.out.println(preferred + " " + other + "|" + result);
		result = Orr.nor(preferred, preferred);
		System.out.println(preferred + " " + preferred + "|" + result);
		System.out.println();
		
		//xor Method
		System.out.println("Xorr Method:");
		result = Xorr.xor(other,  other);
		System.out.println(other + " " + other + "|" + result);
		result = Xorr.xor(other,  preferred);
		System.out.println(other + " " + preferred + "|" + result);
		result = Xorr.xor(preferred,  other);
		System.out.println(preferred + " " + other + "|" + result);
		result = Xorr.xor(preferred, preferred);
		System.out.println(preferred + " " + preferred + "|" + result);
		System.out.println();
		
		//xnor Method
		System.out.println("Xorr Method:");
		result = Xorr.xnor(other,  other);
		System.out.println(other + " " + other + "|" + result);
		result = Xorr.xnor(other,  preferred);
		System.out.println(other + " " + preferred + "|" + result);
		result = Xorr.xnor(preferred,  other);
		System.out.println(preferred + " " + other + "|" + result);
		result = Xorr.xnor(preferred, preferred);
		System.out.println(preferred + " " + preferred + "|" + result);
		System.out.println();
		
		//Decoder Method
		System.out.println("Decoder Method:");
		result = Decoder.decoder(other, other);
		System.out.println(other + " " + other + "|" + result);
		result = Decoder.decoder(other, preferred);
		System.out.println(other + " " + preferred + "|" + result);
		result = Decoder.decoder(preferred, other);
		System.out.println(preferred + " " + other + "|" + result);
		result = Decoder.decoder(preferred, preferred);
		System.out.println(preferred + " " + preferred + "|" + result);
		System.out.println();
		
		//Encoder Method
		System.out.println("Encoder Method:");
		result = Encoder.encoder(other, other, other, preferred);
		System.out.println(other + " " + other + " " + other + " " + preferred + "|" + result);
		result = Encoder.encoder(other, other, preferred, other);
		System.out.println(other + " " + other + " " + preferred + " " + other + "|" + result);
		result = Encoder.encoder(other, preferred, other, other);
		System.out.println(other + " " + preferred + " " + other + " " + other + "|" + result);
		result = Encoder.encoder(preferred, other, other, other);
		System.out.println(preferred + " " + other + " " + other + " " + other + "|" + result);
		System.out.println();
		
		//Latch
		System.out.println("Latch:");
		System.out.println("A B | CD");
		result = Latch.latch(other, other);
		System.out.println(other + " " + other + " | " + result + "   not desired to have D same as C");
		result = Latch.latch(other, preferred);
		System.out.println(other + " " + preferred + " | " + result + "   C is 'set' preferred");
		result = Latch.latch(preferred, preferred);
		System.out.println(preferred + " " + preferred + " | " + result + "   no change; C remembers previous");
		result = Latch.latch(preferred, other);
		System.out.println(preferred + " " + other + " | " + result + "   C is 'Reset' to other");
		result = Latch.latch(preferred, preferred);
		System.out.println(preferred + " " + preferred + " | " + result + "   no change; C remembers previous");
		result = Latch.latch(other, preferred);
		System.out.println(other + " " + preferred + " | " + result + "   C is 'set' preferred");
		result = Latch.latch(preferred, preferred);
		System.out.println(preferred + " " + preferred + " | " + result + "   no change; C remembers previous");
	
		result = ParallelAcessShiftRegister.Pasr(other, preferred, preferred, other, preferred, other, preferred, preferred);
		System.out.println(result);
	}	

}
