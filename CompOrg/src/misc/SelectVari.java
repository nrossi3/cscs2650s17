package misc;

// The purpose of this class is to provide
// the variables for preferred and other.
// To change the value of preferred and other,
// change the value in between the " ".
public class SelectVari {

	public String preferred;
	public String other;
	
	public SelectVari() {
		this.preferred = "1"; //Preferred is stored here
		this.other     = "0"; //Other is stored here
	}
}
