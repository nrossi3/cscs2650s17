package misc;

import flipFlop.MSFlipFlop;
import logicMethods.*;

public class ParallelAcessShiftRegister {

	public static String Pasr(String input0, String input1, String input2, String input3, String mode, String serial, String clockRight, String clockLeft) {
		String result = null;
		String output0, output1, output2, output3, clockout, tempout;
		
		//Setting the clock output
		clockout = Orr.or(Andd.and(Nott.not(mode), clockRight), Andd.and(mode, clockLeft));
		
		//Input section 1
		tempout = Orr.nor(Andd.and(serial, Nott.not(mode)), Andd.and(mode, input0));
		output0 = MSFlipFlop.msflip(tempout, Nott.not(tempout), clockout);

		//Input section 2
		output0 = output0.substring(0, 1);
		tempout = Orr.nor(Andd.and(output0, Nott.not(mode)), Andd.and(mode, input1));
		output1 = MSFlipFlop.msflip(tempout, Nott.not(tempout), clockout);
		
		//Input section 3
		output1 = output1.substring(0, 1);
		tempout = Orr.nor(Andd.and(output1, Nott.not(mode)), Andd.and(mode, input2));
		output2 = MSFlipFlop.msflip(tempout, Nott.not(tempout), clockout);
		
		//Input section 4
		output2 = output2.substring(0, 1);
		tempout = Orr.nor(Andd.and(output2, Nott.not(mode)), Andd.and(mode, input3));
		output3 = MSFlipFlop.msflip(tempout, Nott.not(tempout), clockout);
		output3 = output3.substring(0, 1);
		
		result = output0 + output1 + output2 + output3;
		
		return result;
	}
}