package flipFlop;
import logicMethods.Andd;
import misc.SelectVari;
import flipFlop.MSvari;
import flipFlop.MSFlipFlop;
public class JKFlipFlop {

	public static String msflip(String input0, String input1, String enable) {
		SelectVari variable = new SelectVari();
		String other = variable.other;
		
		String result = null;
		String inputa, inputb;
		
		inputa = MSvari.getms1();
		inputb = MSvari.getms2();
		
		if(inputa == null){inputa = other;}
		if(inputb == null){inputb = other;}
		
		result = MSFlipFlop.msflip(Andd.and(inputb, input0), Andd.and(input1, inputa), enable);
		
		
		return result;

	}

}
