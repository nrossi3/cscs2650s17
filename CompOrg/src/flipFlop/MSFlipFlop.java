package flipFlop;

import latches.EnabledLatch;
import logicMethods.Nott;

public class MSFlipFlop {

	public static String msflip(String input0, String input1, String enable) {
		
		String result = null;
		String output;
		
		output = EnabledLatch.Elatch(input0, input1, enable);
		
		input0 = output.substring(0, 1);
		input1 = output.substring(1, 2);

		enable = Nott.not(enable);
		
		result = EnabledLatch.Elatch(input0, input1, enable);
	
		MSvari vari = new MSvari();
		vari.setms1(result.substring(0, 1));
		vari.setms2(result.substring(1, 2));
		
		
		
		return result;
	}

}
