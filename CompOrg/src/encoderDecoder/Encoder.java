package encoderDecoder;

import logicMethods.Andd;
import logicMethods.Nott;
import logicMethods.Xorr;

public class Encoder {

	public static String encoder(String input3, String input2, String input1, String input0) {
		
		String output0, output1;
		String result = null;
		
		output0 = Xorr.xor(Andd.and(input3, Nott.not(input2)), Andd.and(input1, Nott.not(input0)));
		output1 = Andd.and(Xorr.xor(input3, input2), Andd.and(Nott.not(input1), Nott.not(input0)));
		
		result = output1 + output0;

		return result;
	}

}
