package encoderDecoder;

import logicMethods.Andd;
import logicMethods.Nott;

public class Decoder {
 
	public static String decoder (String input1, String input0){
	
	String output0, output1, output2, output3;
	String result = null;
	
	output0 = Andd.and(Nott.not(input0), Nott.not(input1));
	output1 = Andd.and(Nott.not(input1), input0);
	output2 = Andd.and(input1, Nott.not(input0));
	output3 = Andd.and(input1, input0);
	
	result = output3 + output2 + output1 + output0;
	
	return result;
	}
}
