import encoderDecoder.Encoder;
import logicMethods.Andd;
import logicMethods.Nott;
import logicMethods.Orr;
import misc.SelectVari;

public class FinalProject {
/*A cockpit annunciator (green) light should indicate proper takeoff
conditions which include: throttle being advanced with speed brakes
down, flaps 7-9, auxiliary power exhaust door closed, and stabilizer
in neutral position.*/
	public static void main(String[] args) {
		SelectVari variable = new SelectVari();
		String preferred =  variable.preferred;
		String other = variable.other;
		
		
		String power_exhaust_door 	= other; //P-open, O-closed
		String left_speed_brake 	= preferred; //P-down, O-up
		String right_speed_brake 	= preferred; //P-down, O-up
		String front_landing_gear 	= preferred; //P-down, O-up
		String left_landing_gear 	= preferred; //P-down, O-up
		String right_landing_gear	= preferred; //P-down, O-up
		String stabilizer_neutral 	= preferred; //P-in position, O-not
		int flaps_selector_switch   = 7;
		String throttle_selector_switch = Encoder.encoder(other, other, preferred, other);

		String takeoff_valid;
		
		power_exhaust_door = Nott.not(power_exhaust_door);
		

		
		String throttle_select_1 = throttle_selector_switch.substring(0, 1);
		String throttle_select_2 = throttle_selector_switch.substring(1, 2);
		
		
		
		takeoff_valid = Andd.and(power_exhaust_door,
						Andd.and(left_speed_brake, 
						Andd.and(right_speed_brake, 
						Andd.and(front_landing_gear, 
						Andd.and(left_landing_gear, 
						Andd.and(right_landing_gear, 
						Andd.and(stabilizer_neutral, 
						Orr.or(throttle_select_1, throttle_select_2))))))));
		
		if(takeoff_valid == preferred){
			System.out.println("Ready for Takeoff: GREEN");
		}
		else if(takeoff_valid == other){
			System.out.println("Ready for Takeoff: RED");
		}else {System.out.println("ERROR");}
	}

}