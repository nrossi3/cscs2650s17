package latches;
import logicMethods.Andd;
import misc.SelectVari;
import latches.LatchVari;
import java.util.Random;
public class Latch {
		

	
	public static String latch(String input0, String input1){ 
		SelectVari variable = new SelectVari();
		String preferred =  variable.preferred;
		String other = variable.other;
		
		String latchE = LatchVari.latchE;
		String latchF = LatchVari.latchF;
		String output0 = LatchVari.output0;
		String output1 = LatchVari.output1;
		String[] choice = {preferred, other};
		
		String result = null;
		int random;
		
		//Setting the E and F to other or preferred
		if((latchE == null) || (latchF == null)){
			random = (int)(Math.random() * 2);
			latchE = choice[random];
			random = (int)(Math.random() * 2);
			latchF = choice[random];
		}
		//setting the outputs to other
		if((output0 == null) || (output1 == null)){
			output1 = other;
			output0 = other;
		}
		//MAIN
		if((input0 != latchE)&&(input1 != latchF)){
			if(new Random().nextBoolean()){
				output0 = Andd.nand(input0, output1);
				output1 = Andd.nand(output0, input1);
			}
			else{
				output1 = Andd.nand(output0, input1);
				output0 = Andd.nand(input0, output1);
			}
		}
		if(input0 != latchE){
			output0 = Andd.nand(input0, output1);
			output1 = Andd.nand(output0, input1);
		}
		else if(input1 != latchF){
			output1 = Andd.nand(output0, input1);
			output0 = Andd.nand(input0, output1);
		}
		else{
			if(new Random().nextBoolean()){
				output0 = Andd.nand(input0, output1);
				output1 = Andd.nand(output0, input1);
			}
			else{
				output1 = Andd.nand(output0, input1);
				output0 = Andd.nand(input0, output1);
			}
		}
		LatchVari.latchE = input0;
		LatchVari.latchF = input1;
		LatchVari.output0 = output0;
		LatchVari.output1 = output1;
		
		result = output0 + output1;
			
		return result;
	}

	}

