package latches;
import latches.Latch;
import logicMethods.Andd;
import logicMethods.Nott;
public class DataLatch {

	public static String Dlatch (String input0, String enable) {

		String result = null;
		String output0;
		
		output0 = Andd.nand(input0, enable);
		
		result = Latch.latch(output0, Nott.not(input0));
			
		return result;
	}

}
