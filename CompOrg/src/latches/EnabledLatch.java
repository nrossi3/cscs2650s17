package latches;
import latches.Latch;
import logicMethods.Andd;
public class EnabledLatch {

	public static String Elatch(String input0, String input1, String enable ) {

		String result = null;
		
		String output0;
		String output1; 
		
		output0 = Andd.nand(input0, enable);
		output1 = Andd.nand(input1, enable);
		
		result = Latch.latch(output0, output1);
		
		return result;
	}

}
