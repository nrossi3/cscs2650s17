package logicMethods;

import misc.SelectVari;

public class Andd {
	
	//AND Function
	public static String and (String choice1, String choice2) {
		
		//Brings in the values for preferred and other
		SelectVari variable = new SelectVari();
		String preferred =  variable.preferred;
		String other = variable.other;
		
		//declares string and sets it to null
		String result = null;

		
		if(choice1.equals(preferred)) {		//If the first value is preferred
			if(choice2.equals(preferred)) {	//and the second value is preferred
				result = preferred;		//output preferred	
			}else{
				result = other;	//output other, First value is preferred, Second value is other
			}
		}
		else { result = other;}	//first value is other, no need to evaluate Second value
		return result;	//return the result

	}
	
	//NAND Function (Not + And)
	public static String nand (String choice1, String choice2){
		
		//Declares string and assigns it to null
		String result = null;
		
		result = Andd.and(choice1, choice2);//Runs choice through And Function
		result = Nott.not(result);	//Nots' the result
		
		
		return result;	//return the result
	}
}

