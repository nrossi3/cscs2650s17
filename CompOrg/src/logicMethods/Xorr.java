package logicMethods;

public class Xorr {

	//XOR Function (One or the other but not both)
	public static String xor(String choice1, String choice2) {

		String result = null;
		String output1;	//assigned for first output of adding
		String output2;	//assigned for second output of adding
		String nChoice1;//assigned to hold the NOT of choice 1
		String nChoice2;//Assigned to hold the NOT of choice2
		
		//ANDing the first section
		nChoice1 = Nott.not(choice1);			//NOT choice 1
		output1 = Andd.and(nChoice1, choice2);	//AND nchoice1 and choice2 together, Store in output1
		
		//ANDing the second section
		nChoice2 = Nott.not(choice2);			//NOT choice 2
		output2 = Andd.and(choice1, nChoice2);	//AND nchoice 2 and choice 1 together, Store in output 2
		
		//OR-ing the outputs together
		result = Orr.or(output1, output2);	//OR the two outputs
		return result;	//return result.
	}
	
	//XNOR Function (XOR + NOT)
	public static String xnor(String choice1, String choice2){
		
		//Declares a string and assigns it to null
		String result = null;
		
		result = Xorr.xor(choice1, choice2);//runs choices through XOR
		result = Nott.not(result);//NOTs' output
		
		return result;	//retun result
	}
	
}
