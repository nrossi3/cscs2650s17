package logicMethods;

import misc.SelectVari;

public class Nott {

	// NOT Function
	public static String not (String choice) {
		
		//Brings in the values for preferred and other
		SelectVari variable = new SelectVari();
		String preferred =  variable.preferred;
		String other = variable.other;

		//Declares a string and assigns it to null
		String result = null;
		
		if(choice.equals(preferred)) {	//if choice is preferred
			result = other;				//result is other
		}
		if(choice.equals(other)) {		//if choice is other
			result = preferred;			//result is preferred
		}
		return result;	//return result
	}
}
