package logicMethods;

import misc.SelectVari;

public class Orr {

	//OR Function (One or the other or both)
	public static String or (String choice1, String choice2) {
		
		//Brings in the values for preferred and other
		SelectVari variable = new SelectVari();
		String preferred =  variable.preferred;
		String other = variable.other;

		//Declares a string and assigns it to null
		String result = null;
		
		
		if(choice1.equals(preferred)) {	//if the first choice is preferred
			result = preferred;			//result is preferred
		}
		else if(choice2.equals(preferred)) {	//if the second choice is preferred
			result = preferred;					//the result is preferred
		}
		else {
			result = other;		//otherwise, it is other
		}
		return result;	//return result
		
		//Note: I see there are some changes I could make to this to make it more efficient
		//but I like to see where I started from so I can see where I can improve.
	}

	//NOR Function
	public static String nor (String choice1, String choice2){
		
		//Declares a String and assigns it to null
		String result = null;
		
		result = Orr.or(choice1, choice2);	//run choices through Or function
		result = Nott.not(result);	//run results through Not function
		
		return result;	//return the result
	}
}